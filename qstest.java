import java.util.Scanner;
public class qstest
{
    public static void main(String[] args)
    {
        
        Scanner sc=new Scanner(System.in);
        int n=0;
        System.out.println("Enter a line of  integers");
        String str=sc.nextLine();
        String strArr[]=str.split("\\s");
        int A[]=new int[strArr.length];
        for(int i=0;i<strArr.length;i++)
        {
        	A[i]=Integer.parseInt(strArr[i]);
        }
        Quicksort.quicksort(A, 0, A.length-1);
        for(int i=0;i<A.length;i++)
        {
            System.out.printf("%d ",A[i]);
        }
    
    }
}

class Quicksort
{
    public static void quicksort(int [] A,int p,int r)
    {
        if(p<r)
        {
            int q=partition(A,p,r);
            quicksort(A,p,q-1);
            quicksort(A,q+1,r);
            
        }
    }
    private static int partition(int A[],int p,int r)
    {
        int i=p-1;
        int x=A[r];
        for(int j=p;j<r;j++)
        {
            if(A[j]<=x)
            {
                i++;
               // System.out.printf("A[r]=%d,A[i]=%d\n ",A[r],A[i]);
                int t=A[i];
                A[i]=A[j];
                A[j]=t;
            }
        }
        int t=A[i+1];
        A[i+1]=A[r];
        A[r]=t;
        return i+1;
    }
}
