#include<iostream>
using namespace std;
void Hanoi(int n,char a,char b,char c)
{
//move n discs from a to c 
	if(n==1)
		cout<<a<<"->"<<c<<endl;
	else 
	{
		Hanoi(n-1,a,c,b);//move n-1 discs from a to b
		cout<<a<<"->"<<c<<endl;//move 1 disc from a to c
		Hanoi(n-1,b,a,c);//move n-1 disc from b to c 

	}
}
int main()
{
	int n;
	cout<<"enter discs number n:";
	cin>>n;
	Hanoi(n,'a','b','c');

	return 0;

}