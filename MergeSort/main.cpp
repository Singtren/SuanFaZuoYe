#include<stdlib.h>
#include<string.h>
#include<stdio.h>
void merge(int *A, int p, int q, int r)
{
	//p-q,q+1-r分别是非递减序列，将这两列数合并成非递减序列
	int *L = (int *)malloc(4 * (q - p + 1));
	int *R = (int *)malloc(4 * (r - q));
	int i, j, k;
	memcpy(L, A + p - 1, 4 * (q - p + 1));
	memcpy(R, A + q, 4 * (r - q));
	i = 0; j = 0;
	for (k = p - 1; k <= r - 1; k++)
		if ((j >= r - q) || (L[i]<R[j] && i<q - p + 1))
		{
			A[k] = L[i];
			i++;
		}
		else
		{
			A[k] = R[j];
			j++;
		}
	free(L);
	free(R);
}
void mergesort(int *A, int p, int r)
{
	int q;
	if (p<r)
	{
		q = (p + r) / 2;
		mergesort(A, p, q);
		mergesort(A, q + 1, r);
		merge(A, p, q, r);
	}

}
int main()
{
	int A[100];
	printf("Please enter a integer sequcence(the length must be less than 100,Ctrl+Z to end):\n");
	int n = 0;
	while (scanf("%d",A+n) == 1)
	{
		n++;
		if (n == 100)
			break;
	}

	mergesort(A, 1,n);
	for (int i = 0; i < n; i++)
		printf("%d ", A[i]);
	system("pause");
	return 0;

}