#include<stdio.h>

typedef struct node
{
	char character;
	float freq;
	struct node *lchild,*rchild,*parent;
}node;
void print(node* node);
void rvisit(node *node);
static float WPL=0;
void Huffman(char charlist[],float freqlist [],int num)
{
	node *nodes=new node[num];
	for(int i=0;i<num;i++)
	{
		nodes[i].character=charlist[i];
		nodes[i].freq=freqlist[i];
		nodes[i].lchild=nullptr;
		nodes[i].rchild=nullptr;
		nodes[i].parent=nullptr;

	}
	//合并到Haffman过程中会产生num-1个新结点
	node *mergenodes=new node[num-1];
	for(int i=0;i<num-1;i++)
	{
		mergenodes[i].character='\0';
		mergenodes[i].freq=0;
		mergenodes[i].lchild=nullptr;
		mergenodes[i].rchild=nullptr;
		mergenodes[i].parent=nullptr;

	}
	for(int i=0;i<num-1;i++)//共num-1次合并
	{
		//未合并到Huffman树的树为mergenode[0]-mergenode[i-1]中parent是null的，nodes[0]-nodes[num-1]中parent是null的
		node *minnode1;//最小结点
		node *minnode2;//第二小结点
		node t={'\0',1e10,nullptr,nullptr,nullptr};
		minnode1=&t;
		minnode2=&t;
		for(int j=0;j<num;j++)
		{
			if(nodes[j].parent==nullptr)
			{
				//printf("%d,",(int)nodes[j].freq);
				if(nodes[j].freq<minnode1->freq)
				{
					minnode2=minnode1;
					minnode1=&nodes[j];
				}
				else if(nodes[j].freq<minnode2->freq)
				{
					minnode2=&nodes[j];
				}
			}
		}
		for(int j=0;j<i;j++)
		{
			if(mergenodes[j].parent==nullptr)
			{
				//printf("%d,",(int)mergenodes[j].freq);
				if(mergenodes[j].freq<minnode1->freq)
				{
					minnode2=minnode1;
					minnode1=&mergenodes[j];
				}
				else if(mergenodes[j].freq<minnode2->freq)
				{
					minnode2=&mergenodes[j];
				}
			}
			
		}
		//printf("\n");
		//合并权值最小的两棵子树
		mergenodes[i].lchild=minnode1;
		mergenodes[i].rchild=minnode2;
		mergenodes[i].freq=minnode1->freq+minnode2->freq;
		minnode1->parent=&mergenodes[i];
		minnode2->parent=&mergenodes[i];
		//printf("left:%d,right:%d\n",(int)(minnode1->freq),(int)(minnode2->freq));
		
	}
	//rvisit(&mergenodes[num-2]);
	print(&mergenodes[num-2]);
	printf("WPL=%f\n",WPL);
	delete [] nodes;
	delete []mergenodes;
}
// void rvisit(node *node)
// {
// 	printf("%d ",(int)node->freq);
// 	if(node->lchild!=nullptr)
// 	{
// 		printf("(");
// 		rvisit(node->lchild);
// 		printf(",");
// 	}
// 	if(node->rchild!=nullptr)
// 	{
		
// 		rvisit(node->rchild);
// 		printf(")");
// 	}
// }

void print(node* node)
{
	static char c[20];
	static int i=-1;
	
	++i;
	if(node->lchild==nullptr)
	{
		printf("%c:",node->character);
		for(int j=0;j<i;j++)
		{
			printf("%c",c[j]);

		}
		WPL+=i*node->freq;
		printf("\n");
	}
	else 
	{
		
		c[i]='0';
		print(node->lchild);
		
		c[i]='1';
		print(node->rchild);
		

	}
	i--;
	
}
int main()
{
	char charlist[]="ABCDEFGHI";
	float freqlist[]={10,20,5,15,8,2,3,7,30};
	Huffman(charlist,freqlist,9);
}